package com.veloz4k.user.ui.activity.card;

import com.veloz4k.user.base.MvpPresenter;


public interface CarsIPresenter<V extends CardsIView> extends MvpPresenter<V> {
    void card();
}
