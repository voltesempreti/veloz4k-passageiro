package com.veloz4k.user.ui.activity.passbook;

import com.veloz4k.user.base.MvpPresenter;

public interface WalletHistoryIPresenter<V extends WalletHistoryIView> extends MvpPresenter<V> {
    void wallet();
}
