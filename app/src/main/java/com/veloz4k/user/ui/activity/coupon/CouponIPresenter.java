package com.veloz4k.user.ui.activity.coupon;

import com.veloz4k.user.base.MvpPresenter;

public interface CouponIPresenter<V extends CouponIView> extends MvpPresenter<V> {
    void coupon();
}
