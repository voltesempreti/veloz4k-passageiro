package com.veloz4k.user.ui.activity.payment;

import com.veloz4k.user.base.MvpView;
import com.veloz4k.user.data.network.model.Card;

import java.util.List;

public interface PaymentIView extends MvpView {

    void onSuccess(Object card);

    void onSuccess(List<Card> cards);

    void onAddCardSuccess(Object cards);

    void onError(Throwable e);

}
