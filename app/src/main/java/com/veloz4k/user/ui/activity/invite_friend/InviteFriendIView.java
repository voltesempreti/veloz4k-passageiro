package com.veloz4k.user.ui.activity.invite_friend;

import com.veloz4k.user.base.MvpView;
import com.veloz4k.user.data.network.model.User;

public interface InviteFriendIView extends MvpView {

    void onSuccess(User user);

    void onError(Throwable e);

}
