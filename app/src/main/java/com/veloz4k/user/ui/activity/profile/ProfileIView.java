package com.veloz4k.user.ui.activity.profile;

import com.veloz4k.user.base.MvpView;
import com.veloz4k.user.data.network.model.User;

public interface ProfileIView extends MvpView {

    void onSuccess(User user);

    void onError(Throwable e);

}
