package com.veloz4k.user.ui.activity.wallet;

import com.veloz4k.user.base.MvpView;
import com.veloz4k.user.data.network.model.AddWallet;
import com.veloz4k.user.data.network.model.WalletResponse;

public interface WalletIView extends MvpView {
    void onSuccess(AddWallet object);
    void onError(Throwable e);
    void onSuccess(WalletResponse response);
}
