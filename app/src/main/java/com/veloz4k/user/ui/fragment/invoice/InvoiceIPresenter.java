package com.veloz4k.user.ui.fragment.invoice;

import com.veloz4k.user.base.MvpPresenter;

import java.util.HashMap;

public interface InvoiceIPresenter<V extends InvoiceIView> extends MvpPresenter<V> {
    void payment(HashMap<String, Object> obj);

    void updateRide(HashMap<String, Object> obj);

    void updatePayment(HashMap<String, Object> obj);

    void virtualChange(Integer id);

}
