package com.veloz4k.user.ui.activity.login;

import com.veloz4k.user.base.MvpView;
import com.veloz4k.user.data.network.model.ForgotResponse;
import com.veloz4k.user.data.network.model.Token;

public interface LoginIView extends MvpView {
    void onSuccess(Token token);

    void onSuccess(ForgotResponse object);

    void onError(Throwable e);
}
