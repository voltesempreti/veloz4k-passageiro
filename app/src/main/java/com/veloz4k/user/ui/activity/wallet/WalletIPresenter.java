package com.veloz4k.user.ui.activity.wallet;

import com.veloz4k.user.base.MvpPresenter;

import java.util.HashMap;

public interface WalletIPresenter<V extends WalletIView> extends MvpPresenter<V> {
    void addMoney(HashMap<String, Object> obj);
    void wallet();
}
