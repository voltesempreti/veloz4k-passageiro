package com.veloz4k.user.ui.activity.location_pick;

import com.veloz4k.user.base.MvpPresenter;

public interface LocationPickIPresenter<V extends LocationPickIView> extends MvpPresenter<V> {
    void address();
}
