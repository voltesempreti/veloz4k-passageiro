package com.veloz4k.user.ui.fragment.dispute;

import com.veloz4k.user.base.MvpView;
import com.veloz4k.user.data.network.model.DisputeResponse;
import com.veloz4k.user.data.network.model.Help;

import java.util.List;

public interface DisputeIView extends MvpView {

    void onSuccess(Object object);

    void onSuccessDispute(List<DisputeResponse> responseList);

    void onError(Throwable e);

    void onSuccess(Help help);
}
