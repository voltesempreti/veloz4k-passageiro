package com.veloz4k.user.ui.activity.setting;

import com.veloz4k.user.base.MvpView;
import com.veloz4k.user.data.network.model.AddressResponse;

public interface SettingsIView extends MvpView {

    void onSuccessAddress(Object object);

    void onLanguageChanged(Object object);

    void onSuccess(AddressResponse address);

    void onError(Throwable e);
}
