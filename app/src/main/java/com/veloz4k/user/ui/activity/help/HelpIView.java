package com.veloz4k.user.ui.activity.help;

import com.veloz4k.user.base.MvpView;
import com.veloz4k.user.data.network.model.Help;

public interface HelpIView extends MvpView {

    void onSuccess(Help help);

    void onError(Throwable e);
}
