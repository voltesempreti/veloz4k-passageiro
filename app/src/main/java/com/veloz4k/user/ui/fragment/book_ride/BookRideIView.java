package com.veloz4k.user.ui.fragment.book_ride;

import com.veloz4k.user.base.MvpView;
import com.veloz4k.user.data.network.model.PromoResponse;


public interface BookRideIView extends MvpView {
    void onSuccess(Object object);

    void onError(Throwable e);

    void onSuccessCoupon(PromoResponse promoResponse);
}
