package com.veloz4k.user.ui.activity.profile_update;

import com.veloz4k.user.base.MvpView;
import com.veloz4k.user.data.network.model.User;

public interface ProfileUpdateIView extends MvpView {

    void onSuccess(User user);

    void onUpdateSuccess(User user);

    void onError(Throwable e);

    void onSuccessPhoneNumber(Object object);

    void onVerifyPhoneNumberError(Throwable e);

}
