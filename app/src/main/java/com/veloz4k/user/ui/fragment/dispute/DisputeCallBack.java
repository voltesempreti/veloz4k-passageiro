package com.veloz4k.user.ui.fragment.dispute;

public interface DisputeCallBack {
    void onDisputeCreated();
}
