package com.veloz4k.user.ui.activity.notification_manager;

import com.veloz4k.user.base.MvpPresenter;

public interface NotificationManagerIPresenter<V extends NotificationManagerIView> extends MvpPresenter<V> {
    void getNotificationManager();
}
