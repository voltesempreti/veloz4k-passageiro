package com.veloz4k.user.ui.activity.invite_friend;

import com.veloz4k.user.base.MvpPresenter;

public interface InviteFriendIPresenter<V extends InviteFriendIView> extends MvpPresenter<V> {
    void profile();
}
