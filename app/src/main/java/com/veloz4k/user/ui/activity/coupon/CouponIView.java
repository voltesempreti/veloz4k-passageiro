package com.veloz4k.user.ui.activity.coupon;

import com.veloz4k.user.base.MvpView;
import com.veloz4k.user.data.network.model.PromoResponse;

public interface CouponIView extends MvpView {
    void onSuccess(PromoResponse object);

    void onError(Throwable e);
}
