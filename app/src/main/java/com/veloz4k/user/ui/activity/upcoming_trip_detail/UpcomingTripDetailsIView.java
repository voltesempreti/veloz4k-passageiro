package com.veloz4k.user.ui.activity.upcoming_trip_detail;

import com.veloz4k.user.base.MvpView;
import com.veloz4k.user.data.network.model.Datum;

import java.util.List;

public interface UpcomingTripDetailsIView extends MvpView {

    void onSuccess(List<Datum> upcomingTripDetails);

    void onError(Throwable e);
}
