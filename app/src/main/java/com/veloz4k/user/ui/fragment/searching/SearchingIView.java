package com.veloz4k.user.ui.fragment.searching;

import com.veloz4k.user.base.MvpView;

public interface SearchingIView extends MvpView {
    void onSuccess(Object object);

    void onError(Throwable e);
}
