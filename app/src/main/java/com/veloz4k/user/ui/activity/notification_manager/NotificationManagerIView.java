package com.veloz4k.user.ui.activity.notification_manager;

import com.veloz4k.user.base.MvpView;
import com.veloz4k.user.data.network.model.NotificationManager;

import java.util.List;

public interface NotificationManagerIView extends MvpView {

    void onSuccess(List<NotificationManager> notificationManager);

    void onError(Throwable e);

}