package com.veloz4k.user.ui.fragment.service_flow;

import com.veloz4k.user.base.MvpPresenter;

public interface ServiceFlowIPresenter<V extends ServiceFlowIView> extends MvpPresenter<V> {

}
