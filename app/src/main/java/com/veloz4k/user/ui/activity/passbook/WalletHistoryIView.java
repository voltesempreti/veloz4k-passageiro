package com.veloz4k.user.ui.activity.passbook;

import com.veloz4k.user.base.MvpView;
import com.veloz4k.user.data.network.model.WalletResponse;

public interface WalletHistoryIView extends MvpView {
    void onSuccess(WalletResponse response);

    void onError(Throwable e);

}
