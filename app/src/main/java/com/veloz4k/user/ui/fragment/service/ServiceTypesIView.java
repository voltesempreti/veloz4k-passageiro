package com.veloz4k.user.ui.fragment.service;

import com.veloz4k.user.base.MvpView;
import com.veloz4k.user.data.network.model.Service;

import java.util.List;

public interface ServiceTypesIView extends MvpView {

    void onSuccess(List<Service> serviceList);

    void onError(Throwable e);

    void onSuccess(Object object);
}
