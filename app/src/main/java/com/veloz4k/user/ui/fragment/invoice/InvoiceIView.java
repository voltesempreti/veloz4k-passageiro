package com.veloz4k.user.ui.fragment.invoice;

import com.veloz4k.user.base.MvpView;
import com.veloz4k.user.data.network.model.Message;

public interface InvoiceIView extends MvpView {

    void onSuccess(Message message);

    void onSuccess(Object o);

    void onSuccessVirtualChange(Object o);

    void onSuccessPayment(Object o);

    void onError(Throwable e);

    void onErrorVirtualChange(Throwable e);

}
