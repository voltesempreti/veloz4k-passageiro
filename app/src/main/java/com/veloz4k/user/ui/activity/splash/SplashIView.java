package com.veloz4k.user.ui.activity.splash;

import com.veloz4k.user.base.MvpView;
import com.veloz4k.user.data.network.model.CheckVersion;
import com.veloz4k.user.data.network.model.Service;
import com.veloz4k.user.data.network.model.User;

import java.util.List;

public interface SplashIView extends MvpView {

    void onSuccess(List<Service> serviceList);

    void onSuccess(User user);

    void onError(Throwable e);

    void onSuccess(CheckVersion checkVersion);
}
